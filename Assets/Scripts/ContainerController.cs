﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContainerController : Interactable
{

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    public override void Interact()
    {
        UI_Manager.instance.ChangeToContainerScreen(this);
    }

    public void TakeItems()
    {
        DestroyNode();
    }
}
