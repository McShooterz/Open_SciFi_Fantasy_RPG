﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    [SerializeField]
    protected InteractiveNode node;

    public delegate void InteractFunction();
    InteractFunction interactFunction;

	// Use this for initialization
	protected virtual void Start ()
    {
        interactFunction = Interact;

        if (node != null)
        {
            node.SetParentInteraction(interactFunction);
        }
    }
	
	// Update is called once per frame
	protected virtual void Update ()
    {
		
	}

    public abstract void Interact();

    protected void DestroyNode()
    {
        Destroy(node.gameObject);
    }
}
