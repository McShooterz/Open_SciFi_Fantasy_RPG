﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    [SerializeField]
    Transform playerCamera;

    [SerializeField]
    NavMeshAgent navMeshAgent;

    [SerializeField]
    Animator animator;

    [SerializeField]
    float movementSpeed;

    [SerializeField]
    float rotationRate;

    [SerializeField]
    float turnAngleThreshold;

    [SerializeField]
    LayerMask colliderLayers;

    [SerializeField]
    InteractiveNode interactTarget;

    bool paused;

    bool disabled;

    Vector3 lastNavAgentVelocity;
    NavMeshPath lastNavAgentPath;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            SetPause(!paused);
        }

        if (paused)
            return;

        UpdateMovement();

        ProcessInteractTarget();
    }

    void UpdateMovement()
    {
        Vector3 desiredDirection = Vector3.zero;

        if (!disabled)
        {
            if (Input.GetMouseButton(0) && Input.GetMouseButton(1))
            {
                desiredDirection += new Vector3(playerCamera.forward.x, 0, playerCamera.forward.z);
            }
            else if (Input.GetKey(KeyCode.W))
            {
                desiredDirection += new Vector3(playerCamera.forward.x, 0, playerCamera.forward.z);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                desiredDirection -= new Vector3(playerCamera.forward.x, 0, playerCamera.forward.z);
            }

            if (Input.GetKey(KeyCode.A))
            {
                desiredDirection -= new Vector3(playerCamera.right.x, 0, playerCamera.right.z);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                desiredDirection += new Vector3(playerCamera.right.x, 0, playerCamera.right.z);
            }
        }

        if (desiredDirection != Vector3.zero)
        {
            interactTarget = null;

            desiredDirection = desiredDirection.normalized;

            RotateTowards(desiredDirection);

            //KeepUpright();

            float angle = Vector3.Angle(transform.forward, desiredDirection);

            if (angle < turnAngleThreshold)
            {
                Ray ray = new Ray(transform.position + new Vector3(0, 0.4f, 0), transform.forward);

                if (Physics.Raycast(ray, 0.5f, colliderLayers))
                {
                    animator.SetBool("Run", false);
                }
                else
                {
                    animator.SetBool("Run", true);
                    navMeshAgent.Move(transform.forward * movementSpeed * Time.deltaTime);                
                }
            }
            else
            {
                animator.SetBool("Run", false);
            }
        }
        else
        {
            animator.SetBool("Run", false);
        }
    }

    void ProcessInteractTarget()
    {
        if (interactTarget != null)
        {
            if (interactTarget.InRange((interactTarget.transform.position - transform.position).sqrMagnitude))
            {
                navMeshAgent.SetDestination(transform.position);
                interactTarget.Interact();
                interactTarget = null;
            }
            else
            {
                navMeshAgent.SetDestination(interactTarget.transform.position);
            }

            if (navMeshAgent.velocity.sqrMagnitude > 0)
            {
                animator.SetBool("Run", true);
            }
            else
            {
                animator.SetBool("Run", false);
            }
        }
        else
        {
            navMeshAgent.ResetPath();
        }
    }

    void RotateTowards(Vector3 direction)
    {
        Quaternion rotation = Quaternion.LookRotation(new Vector3(direction.x, transform.position.y, direction.z), Vector3.up);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, rotationRate * Time.deltaTime);
    }

    public void SetPause(bool state)
    {
        paused = state;
        animator.enabled = !state;

        if(paused)
        {
            lastNavAgentPath = navMeshAgent.path;
            lastNavAgentVelocity = navMeshAgent.velocity;
            navMeshAgent.velocity = Vector3.zero;
            navMeshAgent.ResetPath();
        }
        else
        {
            navMeshAgent.velocity = lastNavAgentVelocity;
            navMeshAgent.SetPath(lastNavAgentPath);
        }
    }

    public void SetDisable(bool state)
    {
        disabled = state;

        if(disabled)
        {
            animator.SetBool("Run", false);
            navMeshAgent.SetDestination(transform.position);
            navMeshAgent.ResetPath();
            interactTarget = null;
        }
    }

    public void SetTargetNode(InteractiveNode node)
    {
        interactTarget = node;
    }

    void OnDrawGizmosSelected()
    {
        if (navMeshAgent.path == null)
            return;

        Gizmos.color = Color.red;

        for(int i = 0; i < navMeshAgent.path.corners.Length; i++)
        {
            if (i == 0)
            {
                Gizmos.DrawLine(transform.position, navMeshAgent.path.corners[i]);
            }
            else
            {
                Gizmos.DrawLine(navMeshAgent.path.corners[i - 1], navMeshAgent.path.corners[i]);
            }
        }
    }
}
