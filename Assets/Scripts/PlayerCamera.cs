﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public static PlayerCamera instance;

    [SerializeField]
    Transform CameraTarget;

    [SerializeField]
    int mouseXSpeedMod = 5;

    [SerializeField]
    int mouseYSpeedMod = 5;

    [SerializeField]
    float MaxViewDistance = 15f;

    [SerializeField]
    float MinViewDistance = 1f;

    [SerializeField]
    int ZoomRate = 20;

    float x = 0.0f;
    float y = 0.0f;

    float distance = 3f;
    float desireDistance;
    float correctedDistance;
    float currentDistance;

    public float cameraTargetHeight = 1.0f;

    //checks if first person mode is on
    bool click = false;
    //stores cameras distance from player
    float curDist = 0;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        Vector3 Angles = transform.eulerAngles;
        x = Angles.x;
        y = Angles.y;
        currentDistance = distance;
        desireDistance = distance;
        correctedDistance = distance;


    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Input.GetMouseButton(1))
        {
            x += Input.GetAxis("Mouse X") * mouseXSpeedMod;
            y -= Input.GetAxis("Mouse Y") * mouseYSpeedMod;
        }
        else if(Input.mousePosition.x < 5f)
        {
            x += mouseXSpeedMod;
        }
        else if(Input.mousePosition.x > Screen.width - 5f)
        {
            x -= mouseXSpeedMod;
        }

        y = Helpers.ClampAngle(y, -15, 25);
        Quaternion rotation = Quaternion.Euler(y, x, 0);

        desireDistance -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * ZoomRate * Mathf.Abs(desireDistance);
        desireDistance = Mathf.Clamp(desireDistance, MinViewDistance, MaxViewDistance);
        correctedDistance = desireDistance;

        Vector3 position = CameraTarget.position - (rotation * Vector3.forward * desireDistance);

        RaycastHit collisionHit;
        Vector3 cameraTargetPosition = new Vector3(CameraTarget.position.x, CameraTarget.position.y + cameraTargetHeight, CameraTarget.position.z);

        bool isCorrected = false;

        if (Physics.Linecast(cameraTargetPosition, position, out collisionHit))
        {
            position = collisionHit.point;
            correctedDistance = Vector3.Distance(cameraTargetPosition, position);
            isCorrected = true;
        }

        if(!isCorrected || correctedDistance > currentDistance)
        {
            currentDistance = Mathf.Lerp(currentDistance, correctedDistance, Time.deltaTime * ZoomRate);
        }
        else
        {
            currentDistance = correctedDistance;
        }

        position = CameraTarget.position - (rotation * Vector3.forward * currentDistance + new Vector3(0, -cameraTargetHeight, 0));

        transform.rotation = rotation;
        transform.position = position;

        float cameraX = transform.rotation.x;

        if (Input.GetMouseButtonDown(2))
        {
            if (click == false)
            {
                click = true;
                curDist = distance;
                distance = distance - distance - 1;
            }
            else
            {
                distance = curDist;
                click = false;
            }
        }

    }
}
