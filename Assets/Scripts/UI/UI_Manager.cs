﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Manager : MonoBehaviour
{
    public static UI_Manager instance;

    [SerializeField]
    PlayScreen playScreen;

    [SerializeField]
    ContainerScreen containerScreen;

    [SerializeField]
    DialogueScreen dialogueScreen;

    bool paused;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start ()
    {
        ChangeToPlayScreen();
    }
	
	// Update is called once per frame
	void Update ()
    {
		if (Input.GetKeyDown(KeyCode.Space))
        {
            paused = !paused;
            SetPauseLabel(paused);
        }
	}

    public void ChangeToPlayScreen()
    {
        playScreen.gameObject.SetActive(true);
        containerScreen.gameObject.SetActive(false);
        dialogueScreen.gameObject.SetActive(false);

        InteractionController.instance.enabled = true;
        PlayerCamera.instance.enabled = true;
        PlayerController.instance.SetDisable(false);
    }

    public void ChangeToContainerScreen(ContainerController container)
    {
        playScreen.gameObject.SetActive(false);
        containerScreen.gameObject.SetActive(true);
        dialogueScreen.gameObject.SetActive(false);

        containerScreen.SetContainer(container);

        InteractionController.instance.enabled = false;
        PlayerCamera.instance.enabled = false;
        PlayerController.instance.SetDisable(true);
    }

    public void ChangeToDialogueScreen()
    {
        playScreen.gameObject.SetActive(false);
        containerScreen.gameObject.SetActive(false);
        dialogueScreen.gameObject.SetActive(true);

        InteractionController.instance.enabled = false;
        PlayerCamera.instance.enabled = false;
        PlayerController.instance.SetDisable(true);
    }

    void SetPauseLabel(bool state)
    {
        playScreen.SetPause(state);
    }
}
