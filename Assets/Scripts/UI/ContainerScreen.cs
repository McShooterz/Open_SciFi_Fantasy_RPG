﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContainerScreen : MonoBehaviour
{
    [SerializeField]
    ContainerController container;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void SetContainer(ContainerController container)
    {
        this.container = container;
    }

    public void ClickGetItemsButton()
    {
        if (container != null)
        {
            print("Take Items");
            container.TakeItems();
        }

        UI_Manager.instance.ChangeToPlayScreen();
    }
}
