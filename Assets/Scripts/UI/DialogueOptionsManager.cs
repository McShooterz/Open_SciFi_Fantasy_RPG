﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueOptionsManager : MonoBehaviour
{
    [SerializeField]
    GameObject dialogueOptionPrefab;

    [SerializeField]
    List<DialogueOption> dialogueOptions = new List<DialogueOption>();

    public delegate void OptionSelected(int index);
    OptionSelected optionSelected;

    // Use this for initialization
    void Start ()
    {
        optionSelected = OptionClicked;

        AddDialogueOption("Test Dialogue Option");
        AddDialogueOption("Test Dialogue Option");
        AddDialogueOption("Test Dialogue Option");
        AddDialogueOption("Test Dialogue Option");
        AddDialogueOption("Test Dialogue Option");
        AddDialogueOption("Test Dialogue Option");
        AddDialogueOption("Test Dialogue Option");
        AddDialogueOption("Test Dialogue Option");
        AddDialogueOption("Test Dialogue Option");
        AddDialogueOption("Test Dialogue Option");
        AddDialogueOption("Test Dialogue Option");
        AddDialogueOption("Test Dialogue Option");
        AddDialogueOption("Test Dialogue Option");
    }
	
	// Update is called once per frame
	void Update ()
    {
        
    }

    public void ClearDialogueOptions()
    {
        foreach(DialogueOption option in dialogueOptions)
        {
            Destroy(option.gameObject);
        }

        dialogueOptions.Clear();
    }

    public void AddDialogueOption(string text)
    {
        GameObject dialogueOptionObject = Instantiate(dialogueOptionPrefab);
        dialogueOptionObject.transform.SetParent(transform);
        dialogueOptionObject.transform.localScale = Vector3.one;

        DialogueOption option = dialogueOptionObject.GetComponent<DialogueOption>();
        option.SetCallBackFunction(optionSelected);
        option.SetIndex(dialogueOptions.Count);
        dialogueOptions.Add(option);
        option.SetText(dialogueOptions.Count.ToString() + ". " + text);
    }

    public void OptionClicked(int index)
    {
        //Testing
        UI_Manager.instance.ChangeToPlayScreen();
    }
}
