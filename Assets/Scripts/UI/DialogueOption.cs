﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueOption : MonoBehaviour
{
    [SerializeField]
    Color normalColor;

    [SerializeField]
    Color hoveredColor;

    [SerializeField]
    int index;

    [SerializeField]
    Text dialogueText;

    DialogueOptionsManager.OptionSelected clickedOption;

    // Use this for initialization
    void Start ()
    {
        dialogueText.color = normalColor;
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void SetCallBackFunction(DialogueOptionsManager.OptionSelected callBack)
    {
        clickedOption = callBack;
    }

    public void SetIndex(int value)
    {
        index = value;
    }

    public int GetIndex()
    {
        return index;
    }

    public void SetText(string text)
    {
        dialogueText.text = text;
    }

    public void MouseEnter()
    {
        dialogueText.color = hoveredColor;
    }

    public void MouseExit()
    {
        dialogueText.color = normalColor;
    }

    public void Clicked()
    {
        clickedOption(index);
    }
}
