﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayScreen : MonoBehaviour
{

    [SerializeField]
    Text pauseLabel;

	// Use this for initialization
	void Start ()
    {
        SetPause(false);
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void SetPause(bool state)
    {
        pauseLabel.gameObject.SetActive(state);
    }
}
