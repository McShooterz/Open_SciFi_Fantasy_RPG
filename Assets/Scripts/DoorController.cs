﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : Interactable
{

    [SerializeField]
    Collider doorCollider;

    [SerializeField]
    float openingTime;

    [SerializeField]
    Animator animator;

    // Use this for initialization
    protected override void Start ()
    {
        base.Start();
	}

    // Update is called once per frame
    protected override void Update ()
    {
        base.Update();
	}

    public override void Interact()
    {
        StartCoroutine(RemoveDoorCollider(openingTime));

        animator.SetBool("Open", true);

        DestroyNode();
    }





    IEnumerator RemoveDoorCollider(float time)
    {
        yield return new WaitForSeconds(time);

        doorCollider.enabled = false;
    }
}
