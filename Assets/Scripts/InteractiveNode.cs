﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveNode : MonoBehaviour
{
    [SerializeField]
    string displayName = "Fill in display name";

    [SerializeField]
    float interactRange = 2f;

    Interactable.InteractFunction interact;

    // Use this for initialization
    void Start ()
    {
        InteractionController.instance.AddInteractiveNode(this);

        // Square to not need square roots
        interactRange *= interactRange;
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void SetParentInteraction(Interactable.InteractFunction interactFunction)
    {
        interact = interactFunction;
    }

    public string GetName()
    {
        return displayName;
    }

    public bool InRange(float range)
    {
        return range < interactRange;
    }

    public void Interact()
    {
        if (interact != null)
        {
            interact();
        }
    }

    void OnDestroy()
    {
        InteractionController.instance.RemoveInteractiveNode(this);
    }
}
