﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionController : MonoBehaviour
{
    public static InteractionController instance;

    [SerializeField]
    LayerMask nodeLayer;

    [SerializeField]
    float nodeDrawDistance;

    [SerializeField]
    Texture hoveredNodeTexture;

    [SerializeField]
    Texture nodeTexture;

    [SerializeField]
    Vector2 nodeSizeDrawn;

    [SerializeField]
    Vector2 nodeNameSizeDrawn;

    Vector2 nodeDrawnOffset;
    Vector2 nodeNameDrawnOffset;

    [SerializeField]
    List<InteractiveNode> interactiveNodes = new List<InteractiveNode>();

    GameObject hoveredObject;

    // UI

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start ()
    {

        // Sqr node draw distance of calcutions
        nodeDrawDistance *= nodeDrawDistance;

        nodeDrawnOffset = new Vector2(nodeSizeDrawn.x / -2f, nodeSizeDrawn.y / -2f);
        nodeNameDrawnOffset = new Vector2(nodeNameSizeDrawn.x / -2f, nodeNameSizeDrawn.y / -2f);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetMouseButton(1))
        {
            Cursor.visible = false;
        }
        else
        {
            Cursor.visible = true;

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.collider.gameObject != hoveredObject)
                {
                    hoveredObject = hit.collider.gameObject;
                }

                if (!Input.GetMouseButton(1) && Input.GetMouseButtonDown(0))
                {
                    InteractiveNode node = hoveredObject.GetComponent<InteractiveNode>();

                    PlayerController.instance.SetTargetNode(node);
                }
            }
            else
            {
                hoveredObject = null;
            }
        }
	}

    void OnGUI()
    {
        // Find the nodes to draw
        foreach (InteractiveNode node in interactiveNodes)
        {
            // Get Screen position
            Vector3 screenPoint = Camera.main.WorldToScreenPoint(node.transform.position);
            screenPoint.y = Screen.height - screenPoint.y;

            // Check if hovered

            if (node.gameObject == hoveredObject)
            {
                GUI.DrawTexture(new Rect(new Vector2(screenPoint.x + nodeDrawnOffset.x, screenPoint.y + nodeDrawnOffset.y), nodeSizeDrawn), hoveredNodeTexture);

                screenPoint.y -= nodeNameSizeDrawn.y * 1.5f;

                GUI.Box(new Rect(new Vector2(screenPoint.x + nodeNameDrawnOffset.x, screenPoint.y + nodeNameDrawnOffset.y), nodeNameSizeDrawn), node.GetName());
            }
            else
            {
                // Check distance
                Vector3 direction = node.transform.position - Camera.main.transform.position;

                if (direction.sqrMagnitude < nodeDrawDistance && RayCastToNode(node, direction) && InCameraView(screenPoint))
                {
                    GUI.DrawTexture(new Rect(new Vector2(screenPoint.x + nodeDrawnOffset.x, screenPoint.y + nodeDrawnOffset.y), nodeSizeDrawn), nodeTexture);
                }
            }
        }
    }

    public void AddInteractiveNode(InteractiveNode node)
    {
        interactiveNodes.Add(node);
    }

    public void RemoveInteractiveNode(InteractiveNode node)
    {
        interactiveNodes.Remove(node);
    }

    public void ClearInteractiveNodes()
    {
        interactiveNodes.Clear();
    }

    bool RayCastToNode(InteractiveNode node, Vector3 direction)
    {
        RaycastHit hit;
        Ray ray = new Ray(Camera.main.transform.position, direction);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, nodeLayer))
        {
            return hit.collider.gameObject == node.gameObject;
        }
        else
        {
            return false;
        }
    }

    bool InCameraView(Vector3 sceenPoint)
    {
        return sceenPoint.z > 0 && sceenPoint.x > 0 && sceenPoint.x < Screen.width && sceenPoint.y > 0 && sceenPoint.y < Screen.width;
    }
}
